Crossentropy loss çaprazentropi kaybı fonksiyonunda L ve N parametreleri şöyle hesaplanır
L Çaprazentropi kaybı optimizasyon fonksiyonunun değeridir Bu değer veri setindeki her nokta için gerçek sınıf değeri ile tahmin edilen sınıf değeri arasındaki farkın toplamını temsil eder Bu değer modelin eğitim sırasında optimize edilir ve en küçük değere getirilir
N  Veri setindeki nokta sayısıdır her nokta için çapraz entropi kaybının ölçülmesi için kullanılır
Formülün içinde i   den N ye kadar döngü yapılır ve her döngüde gerçek sınıf değerleri ile tahmin edilen sınıf değerleri arasındaki farkın kareleri toplanır Bu toplanan değer sonrasında Ne bölünür ve loss değeri elde edilir Bu değer eğitim sırasında modelin parametreleri optimize edilir ve en küçük değere getirilir
Fri Jan    GMT GMT
httpschatopenaicomchat__cf_chl_tkhbwnVxBiEPIauCsZclxAaEZoMe_tIbEhYogaNycGzNEE