CRISPDM avantaj ve dezavantaj 
Cross Industry Standard Process for Data Mining veri madenciliği için yaygın olarak kullanılan bir yöntemdir ve aşağıdaki avantajları ve dezavantajları vardır
Avantajlar
CRISPDM veri madenciliği sürecini kolay anlaşılır ve sistematik bir şekilde ele alır
CRISPDM veri madenciliği projelerinin yürütülmesi için gereken tüm aşamaları kapsar ve bu nedenle projelerin daha yüksek başarı oranlarına ulaşmasına yardımcı olur
CRISPDM veri madenciliği projelerinin yürütülmesi için gerekli olan tüm kaynakların insan teknoloji veri etkili bir şekilde yönetilmesini sağlar
CRISPDM veri madenciliği projelerinde tüm taraflar arasında işbirliğini ve işbirliği için gerekli olan komunikasyonu sağlar
Dezavantajlar
CRISPDM veri madenciliği projelerinin yürütülmesi için gereken bütçe ve zaman gibi kaynakların yüksek olmasını gerektirir
CRISPDM veri madenciliği projelerinde yer alan tarafların yeterli seviyede veri madenciliği bilgisine sahip olmasını gerektirir
CRISPDM veri madenciliği projelerinde ihtiyacı olan veri miktarının büyüklüğünden dolayı bazen zorluklar oluşabilir
CRISPDM veri madenciliği projelerinde çok fazla zaman ve çaba gerektirecek ayrıntılı bir planlama ve dokümantasyon gerektirir
Sat Jan    GMT GMT